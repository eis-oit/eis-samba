
Simple image that runs Alpine Linux 3.12 + Samba Utils

The following packages are pre-installed:
- bash (via eis-alpine)
- vim (via eis-alpine)
- samba

Default command is bash

Examples:
* Build this image:
`docker build -t eis/samba .`

* Run image:
`docker run --rm -ti eis/samba`

* Pull this image from the EIS Container Registry
`docker pull gitlab-registry.oit.duke.edu/eis-oit/images/eis-samba:main`

* Run from container registry:
`docker run --rm -it gitlab-registry.oit.duke.edu/eis-oit/images/eis-samba:main`

* Use this as a starting point for another project in your Dockerfile:
`FROM gitlab-registry.oit.duke.edu/eis-oit/images/eis-samba:main`
