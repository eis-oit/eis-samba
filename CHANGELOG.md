# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project mostly adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).
The first part of the versioning is the alpine version followed by Semantic Versioning


## [1.0.0] - 2021-11-05

### Added
- Initial Image Release/Build