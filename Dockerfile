FROM gitlab-registry.oit.duke.edu/eis-oit/images/eis-alpine:main
LABEL maintainer="jeremy.hopkins@duke.edu"

RUN apk update \
    && \
    apk add --no-cache \
        samba

CMD ["/bin/bash"]
